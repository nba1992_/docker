IMAGE_NAME="bitnami/mariadb:latest"
CONTAINER_NAME="nba_mariadb"
CONTAINERS_PORTS=""
CONTAINER_VOLUMES="-v /d:/docker_volumes/mariadb:/bitnami"
CONTAINER_EXTRA_ARGS="-e ALLOW_EMPTY_PASSWORD=yes"
sh ../../../../scripts/cli/linux/createContainer.sh $IMAGE_NAME $CONTAINER_NAME $CONTAINERS_PORTS $CONTAINER_VOLUMES $CONTAINER_EXTRA_ARGS

IMAGE_NAME="bitnami/phabricator:latest"
CONTAINER_NAME="nba_phabricator"
CONTAINERS_PORTS="-p 80:80 -p 443:443"
CONTAINER_VOLUMES="-v /d:/docker_volumes/phabricator:/bitnami"
CONTAINER_EXTRA_ARGS="-e ALLOW_EMPTY_PASSWORD=yes"
sh ../../../../scripts/cli/linux/createContainer.sh $IMAGE_NAME $CONTAINER_NAME $CONTAINERS_PORTS $CONTAINER_VOLUMES $CONTAINER_EXTRA_ARGS