COMPOSE_FILE="../../docker-compose.yaml"
ENV_FILE="../../.env"

sh ../../../../scripts/compose/linux/startCompose.sh "$COMPOSE_FILE" "$ENV_FILE"