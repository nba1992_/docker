# Docker

Docker is a repository created with a plug and play idea, in order to accelerate implementation.
Of course, you have to have docker installed locally to interact with docker engine.
[Docker download](https://hub.docker.com/search?offering=community&q=&type=edition)

## Installation

For each images/*, there is script files(cleanInstallCompose.bat, cleanInstall.sh, etc) that creates everything necessary in your local docker engine.
Each one of these scripts, invoke global and generic scripts located at scripts/*.

## Usage

* Refactor "D:/repos/docker" to the full path where you clone the repository.

### Compose

```script
#windows compose scripts
cd images/artifactory/compose/windows
cleanInstallCompose.bat
```

```script
#linux compose scripts
cd images/artifactory/compose/linux
sh cleanInstallCompose.sh
```

```script
#pwsh compose scripts
cd images/artifactory/compose/pwsh
sh cleanInstallCompose.ps1
```

### CLI

```script
#windows cli scripts
cd images/artifactory/cli/windows
cleanInstallCompose.bat
```

```script
#linux cli scripts
cd images/artifactory/cli/linux
sh cleanInstallCompose.sh
```

```script
#pwsh cli scripts
cd images/artifactory/cli/pwsh
sh cleanInstallCompose.ps1
```

## Images/Tools/Apps

* Artifactory
  * [Home](https://jfrog.com/artifactory/)
  * [Working in Docker](https://www.jfrog.com/confluence/display/RTF6X/Installing+with+Docker)

* Centos
  * [Home](https://www.centos.org/)
  * [Working in Docker](https://www.cyberithub.com/how-to-build-docker-image-from-dockerfile-in-centos-8-example/)

* elastik stack
    * [Home](xxx)
    * [Working in Docker](xxx)

* Gitlab
    * [Home](https://about.gitlab.com/)
    * [Working in Docker](https://docs.gitlab.com/ee/install/docker.html)

* Harbor
    * [Home](https://goharbor.io/)
    * [Working in Docker](https://goharbor.io/docs)

* Jenkins
    * [Home](https://www.jenkins.io/)
    * [Working in Docker](https://www.jenkins.io/doc/book/installing/docker/)

* Jenkins Blue Ocean
    * [Home](https://www.jenkins.io/doc/book/blueocean/)
    * [Working in Docker](https://hub.docker.com/r/jenkinsci/blueocean/)

* kics
    * [Home](xxx)
    * [Working in Docker](xxx)

* Nexus
    * [Home](https://www.jenkins.io/doc/book/blueocean/)
    * [Working in Docker](https://blog.sonatype.com/sonatype-nexus-installation-using-docker)

* owasp zap
    * [Home](https://www.zaproxy.org/)
    * [Working in Docker](https://www.zaproxy.org/docs/docker/)

* phabricator
    * [Home](xxx)
    * [Working in Docker](xxx)

* portainer
    * [Home](xxx)
    * [Working in Docker](xxx)

* selenium
    * [Home](xxx)
    * [Working in Docker](xxx)

* sonarqube
    * [Home](xxx)
    * [Working in Docker](xxx)

* sonarwhal
    * [Home](xxx)
    * [Working in Docker](xxx)

* swagger-ui
    * [Home](xxx)
    * [Working in Docker](xxx)

* ubuntu
    * [Home](xxx)
    * [Working in Docker](xxx)

* vagrant
    * [Home](xxx)
    * [Working in Docker](xxx)

* websphere
    * [Home](xxx)
    * [Working in Docker](xxx)

* wso2am
    * [Home](xxx)
    * [Working in Docker](xxx)

* xxx
    * [Home](xxx)
    * [Working in Docker](xxx)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

FREE
