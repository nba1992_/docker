echo off
echo # # # # # # # # # # # # # # #
echo ----- Create Container ------
echo # # # # # # # # # # # # # # #

set ARGUMENTS=5

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%	
    exit 1
)


set IMAGE_NAME=%1
set CONTAINER_NAME=%2
set CONTAINER_PORTS=%3
set CONTAINERS_VOLUMES=%4
set CONTAINER_EXTRA_ARGS=%5

docker run -d --restart unless-stopped --name %CONTAINER_NAME% %CONTAINER_PORTS% %CONTAINERS_VOLUMES% %CONTAINER_EXTRA_ARGS% %IMAGE_NAME%