echo off
echo # # # # # # # # # # # # # 
echo ----- Create Image ------
echo # # # # # # # # # # # # # 

set ARGUMENTS=2

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%	
    exit 1
)


set IMAGE_NAME=%1
set DOCKERFILE=%2

docker build --rm --no-cache -t %IMAGE_NAME% %DOCKERFILE%