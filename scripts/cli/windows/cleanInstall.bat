echo off
echo # # # # # # # # # # # # # 
echo ----- Clean Install -----
echo # # # # # # # # # # # # # 

set ARGUMENTS=6

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%	
    exit 1
)

set IMAGE_NAME=%1
set CONTAINER_NAME=%2
set CONTAINER_PORTS=%3
set CONTAINER_VOLUMES=%4
set CONTAINER_EXTRA_ARGS=%5
set DOCKERFILE=%6

call stopContainer.bat "%CONTAINER_NAME%"
call deleteContainer.bat "%CONTAINER_NAME%"
call deleteImage.bat "%IMAGE_NAME%"
call createImage.bat "%IMAGE_NAME%" "%DOCKERFILE%"
call createContainer.bat "%IMAGE_NAME%" "%CONTAINER_NAME%" "%CONTAINER_PORTS%" "%CONTAINER_VOLUMES%" "%CONTAINER_EXTRA_ARGS%"
