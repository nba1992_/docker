echo off
echo # # # # # # # # # # # # # # #
echo ----- Exec Command Container ------
echo # # # # # # # # # # # # # # #

set ARGUMENTS=2

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%	
    exit 1
)

set CONTAINER_NAME=%1
set EXEC_COMMAND=%2


docker exec -it %CONTAINER_NAME% %EXEC_COMMAND%