echo off
echo # # # # # # # # # # # # # # #
echo ----- Logs Container ------
echo # # # # # # # # # # # # # # #

set ARGUMENTS=1

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%	
    exit 1
)

set CONTAINER_NAME=%1


docker logs -f --tail=all %CONTAINER_NAME%