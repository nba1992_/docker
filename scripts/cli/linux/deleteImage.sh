#!/bin/bash
echo # # # # # # # # # # # # # 
echo ----- Delete Image ------
echo # # # # # # # # # # # # # 

ARGUMENTS=1
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters missing: #'$#' should be '$ARGUMENTS	
    exit 1
fi

IMAGE_NAME=$1

docker rmi $IMAGE_NAME