#!/bin/bash
echo # # # # # # # # # # # # # 
echo ----- Create Image ------
echo # # # # # # # # # # # # # 

ARGUMENTS=2
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters missing: #'$#' should be '$ARGUMENTS	
    exit 1
fi

IMAGE_NAME=$1
DOCKERFILE=$2

docker build --rm --no-cache -t $IMAGE_NAME $DOCKERFILE