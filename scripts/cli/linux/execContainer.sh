#!/bin/bash
echo # # # # # # # # # # # # # # #
echo ----- Exec Command Container ------
echo # # # # # # # # # # # # # # #

ARGUMENTS=2
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters missing: #'$#' should be '$ARGUMENTS	
    exit 1
fi

CONTAINER_NAME=$1
EXEC_COMMAND=$2


docker exec -it $CONTAINER_NAME $EXEC_COMMAND