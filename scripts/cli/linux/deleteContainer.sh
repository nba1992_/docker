#!/bin/bash
echo # # # # # # # # # # # # # # #
echo ----- Delete Container ------
echo # # # # # # # # # # # # # # #

ARGUMENTS=1
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters missing: #'$#' should be '$ARGUMENTS	
    exit 1
fi

CONTAINER_NAME=$1


docker rm $CONTAINER_NAME