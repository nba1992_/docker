#!/bin/bash
echo # # # # # # # # # # # # # 
echo ----- Clean Install -----
echo # # # # # # # # # # # # # 

ARGUMENTS=6
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
echo 
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters error: Current value is #'$#', but should be '$ARGUMENTS	
    exit 1
fi

IMAGE_NAME=$1
CONTAINER_NAME=$2
CONTAINER_PORTS=$3
CONTAINER_VOLUMES=$4
CONTAINER_EXTRA_ARGS=$5
DOCKERFILE=$6
SCRIPTS_HOME="/D/repos/docker/scripts/cli/linux"


sh $SCRIPTS_HOME/stopContainer.sh "$CONTAINER_NAME"
sh $SCRIPTS_HOME/deleteContainer.sh "$CONTAINER_NAME"
sh $SCRIPTS_HOME/deleteImage.sh "$IMAGE_NAME"
sh $SCRIPTS_HOME/createImage.sh "$IMAGE_NAME" "$DOCKERFILE"
sh $SCRIPTS_HOME/createContainer.sh "$IMAGE_NAME" "$CONTAINER_NAME" "$CONTAINER_PORTS" "$CONTAINER_VOLUMES" "$CONTAINER_EXTRA_ARGS"
#sh $SCRIPTS_HOME/logsContainer.sh "$CONTAINER_NAME"