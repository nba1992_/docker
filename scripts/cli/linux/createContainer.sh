#!/bin/bash
echo # # # # # # # # # # # # # # #
echo ----- Create Container ------
echo # # # # # # # # # # # # # # #

ARGUMENTS=5
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters missing: #'$#' should be '$ARGUMENTS	
    exit 1
fi

IMAGE_NAME=$1
CONTAINER_NAME=$2
CONTAINER_PORTS=$3
CONTAINERS_VOLUMES=$4
CONTAINER_EXTRA_ARGS=$5

docker run -d --restart unless-stopped --name $CONTAINER_NAME $CONTAINER_PORTS $CONTAINERS_VOLUMES $CONTAINER_EXTRA_ARGS $IMAGE_NAME