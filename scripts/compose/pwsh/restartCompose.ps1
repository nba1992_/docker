param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile,

    [Parameter(Mandatory = $true)]
    [string] $envFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Restart Compose"

Write-Host "docker-compose --verbose --file $composeFile --env-file $envFile restart"
docker-compose --verbose --file $composeFile --env-file $envFile restart