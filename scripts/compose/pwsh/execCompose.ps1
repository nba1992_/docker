param (
    [Parameter(Mandatory = $true)]
    [string] $command
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Exec Compose"

Write-Host "docker-compose --verbose exec $command"
docker-compose --verbose exec "$command"