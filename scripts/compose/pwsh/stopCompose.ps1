param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile,

    [Parameter(Mandatory = $true)]
    [string] $envFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Stop Compose"

Write-Host "docker-compose --verbose --file $composeFile --env-file $envFile stop"
docker-compose --verbose --file $composeFile --env-file $envFile stop