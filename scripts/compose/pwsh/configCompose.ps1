param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message " Config Compose"

Write-Host "docker-compose --verbose --file $composeFile config"
docker-compose --verbose --file $composeFile config