param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Logs Compose"

Write-Host "docker-compose --verbose --file $composeFile logs -f"
docker-compose --verbose --file $composeFile logs -f