param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Down Compose"

Write-Host "docker-compose --verbose --file $composeFile down --rmi all"
docker-compose --verbose --file $composeFile down --rmi all