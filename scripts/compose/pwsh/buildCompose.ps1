param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Build Compose"

Write-Host "docker-compose --verbose --file $composeFile build --pull --no-cache --force-rm"
docker-compose --verbose --file $composeFile build --pull --no-cache --force-rm

