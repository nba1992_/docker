param (
    [Parameter(Mandatory = $true)]
    [string] $composeFile,

    [Parameter(Mandatory = $true)]
    [string] $envFile
)

$toolsDir = "D:/repos/docker/tools/pwsh"
$scriptsDir = "D:/repos/docker/scripts/compose/pwsh"

pwsh ${toolsDir}/printTitle.ps1 -message "Clean Install Compose"

pwsh ${scriptsDir}/configCompose.ps1 $composeFile
pwsh ${scriptsDir}/stopCompose.ps1 $composeFile $envFile
pwsh ${scriptsDir}/downCompose.ps1 $composeFile
pwsh ${scriptsDir}/buildCompose.ps1 $composeFile
pwsh ${scriptsDir}/upCompose.ps1 $composeFile $envFile
pwsh ${scriptsDir}/startCompose.ps1 $composeFile $envFile