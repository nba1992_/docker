echo off
echo # # # # # # # # # # # # #
echo ----- Clean Install Compose -----
echo # # # # # # # # # # # # #

set ARGUMENTS=2

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set COMPOSE_FILE=%1
set ENV_FILE=%2
set SCRIPTS_HOME=D:/repos/docker/scripts/compose/windows

call %SCRIPTS_HOME%\configCompose.bat %COMPOSE_FILE%
call %SCRIPTS_HOME%\stopCompose.bat %COMPOSE_FILE% %ENV_FILE%
call %SCRIPTS_HOME%\downCompose.bat %COMPOSE_FILE%
call %SCRIPTS_HOME%\buildCompose.bat %COMPOSE_FILE%
call %SCRIPTS_HOME%\upCompose.bat %COMPOSE_FILE% %ENV_FILE%
call %SCRIPTS_HOME%\startCompose.bat %COMPOSE_FILE% %ENV_FILE%