echo off
echo # # # # # # # # # # # # #
echo ----- Build Compose -----
echo # # # # # # # # # # # # #

set ARGUMENTS=1

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set COMPOSE_FILE=%1

echo docker-compose --verbose --file %COMPOSE_FILE% build --pull --no-cache --force-rm
docker-compose --verbose --file %COMPOSE_FILE% build --pull --no-cache --force-rm