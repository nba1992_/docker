echo off
echo # # # # # # # # # # # # #
echo ----- Up Compose -----
echo # # # # # # # # # # # # #

set ARGUMENTS=2

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set COMPOSE_FILE=%1
set ENV_FILE=%2

echo docker-compose --verbose --file %COMPOSE_FILE% --env-file %ENV_FILE% up -d --build
docker-compose --verbose --file %COMPOSE_FILE% --env-file %ENV_FILE% up -d --build