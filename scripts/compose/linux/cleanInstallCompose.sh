#!/bin/bash
echo # # # # # # # # # # # # #
echo ----- Clean Install Compose -----
echo # # # # # # # # # # # # #

ARGUMENTS=2
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
echo
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters error: Current value is #'$#', but should be '$ARGUMENTS
    exit 1
fi

COMPOSE_FILE=$1
ENV_FILE=$2
SCRIPTS_HOME="/D/repos/docker/scripts/compose/linux"

sh $SCRIPTS_HOME/configCompose.sh "$COMPOSE_FILE"
sh $SCRIPTS_HOME/stopCompose.sh "$COMPOSE_FILE" "$ENV_FILE"
sh $SCRIPTS_HOME/downCompose.sh "$COMPOSE_FILE"
sh $SCRIPTS_HOME/buildCompose.sh "$COMPOSE_FILE"
sh $SCRIPTS_HOME/upCompose.sh "$COMPOSE_FILE" "$ENV_FILE"
sh $SCRIPTS_HOME/startCompose.sh "$COMPOSE_FILE" "$ENV_FILE"