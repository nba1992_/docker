#!/bin/bash
echo # # # # # # # # # # # # #
echo ----- Logs Compose -----
echo # # # # # # # # # # # # #

ARGUMENTS=1
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
echo
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters error: Current value is #'$#', but should be '$ARGUMENTS
    exit 1
fi

COMPOSE_FILE=$1

docker-compose --verbose --file "$COMPOSE_FILE" logs -f