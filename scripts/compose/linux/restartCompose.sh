#!/bin/bash
echo # # # # # # # # # # # # #
echo ----- Restart Compose -----
echo # # # # # # # # # # # # #

ARGUMENTS=2
for SOME_VAR in "$@"
do
    echo "$SOME_VAR"
done;
echo
if [ $# -ne $ARGUMENTS ] ; then
    echo 'Parameters error: Current value is #'$#', but should be '$ARGUMENTS
    exit 1
fi

COMPOSE_FILE=$1
ENV_FILE=$2

docker-compose --verbose --file "$COMPOSE_FILE" --env-file "$ENV_FILE" restart