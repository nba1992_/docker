#!/bin/bash

sh printTitle.sh  "Full Clean Docker"

sh cleanDocker.sh
# Delete all containers
echo "Delete all containers"; docker rm -f $(docker ps -a -q); echo "done"
# Delete all images
echo "Delete all images"; docker rmi -f $(docker images -q); echo "done"