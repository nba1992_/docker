#!/bin/bash

sh printTitle.sh  "Update Latest Images"

docker images --format "{{.Repository}}:{{.Tag}}" | grep :latest | xargs -L1 docker pull