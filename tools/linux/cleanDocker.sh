#!/bin/bash

sh printTitle.sh "Clean Docker"

# Remove containers with status "exited"
echo "Removing containers with status exited..."; docker rm -v $(docker ps -a -f status=exited --format '{{.Names}}'); echo "done"

# Remove untagged images aka "dangling images" (<none> : <none>)
echo "Removing untagged images : <none> tagged images..."; docker rmi -f $(docker images -f "dangling=true" --format '{{.Names}}'); echo "done"

# Remove Volumes
echo "Cleaning volumes..."; docker volume rm $(docker volume ls -qf dangling=true); echo "done"
