#!/bin/bash
echo # # # # # # # # # # # # # # # # # # # # # # # # # #
echo ---------------------------------------------------
echo Starting installing Projects
echo ---------------------------------------------------
echo # # # # # # # # # # # # # # # # # # # # # # # # # #

array=(jenkins sonarqube)

COLLECTION="$(ls -d */)"
#COLLECTION="${array[@]}"

for i in $COLLECTION
	do 
		echo ${i%%/};
		if [ ${i%%/} != 'Volumes' -a ${i%%/} != 'tools' -a ${i%%/} != 'scripts' ] ; then
			sh ${i%%/}/cleanInstall.sh
		fi
done


echo # # # # # # # # # # # # # # # # # # # # # # # # # #
echo ---------------------------------------------------
echo Finish installing Projects
echo ---------------------------------------------------
echo # # # # # # # # # # # # # # # # # # # # # # # # # #
