#!/bin/bash

sh printTitle.sh  "Update All Images"

docker images |grep -v REPOSITORY|awk '{print $1}'|xargs -L1 docker pull