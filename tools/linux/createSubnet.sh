#!/bin/bash

sh printTitle.sh  "Create Docker Subnet"

docker network create --subnet 172.20.0.0/16 nba-subnet
