#!/bin/bash

sh printTitle.sh  "Update All Images"

docker images | awk '(NR>1) && ($2!~/none/) {print $1":"$2}' | xargs -L1 docker pull