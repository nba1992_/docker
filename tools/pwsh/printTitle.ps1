param (
    [Parameter(Mandatory = $true)]
    [string] $message
)

Write-Host "# # # # # # # # # # # # # # # # # # # # # # # # # #"
Write-Host "---------------------------------------------------"
Write-Host "$message"
Write-Host "---------------------------------------------------"
Write-Host "# # # # # # # # # # # # # # # # # # # # # # # # # #"